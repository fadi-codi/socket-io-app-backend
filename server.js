import express from 'express';
import dotenv from 'dotenv';
import connectDB from './config/db.js';
import { errorHandler, notFound } from './middleware/errorMiddleware.js';
import cors from 'cors'
import { Server } from "socket.io";
import userRoutes from './routes/userRoutes.js';
import chatRoutes from "./routes/chatRoutes.js";
import messageRoutes  from "./routes/messageRoutes.js";

dotenv.config();

connectDB();

const PORT = process.env.PORT || 5000;

const app = new express();

app.use(cors())

app.use(express.json());



// app.use(notFound);
// app.use(errorHandler);

app.use("/api/user", userRoutes);
app.use("/api/chat", chatRoutes);
app.use("/api/message", messageRoutes);


const server =app.listen(
  PORT,
  console.log(
    `Server Running on Port ${PORT}`
  )
);

var usersOnline = []

const io = new Server(server, {
    pingTimeout: 60000,
    cors: {
      origin: "http://localhost:3000",
 
    },
  });
 
  io.on("connection", (socket) => {
   console.log("user connected");
 

    socket.on("setup", (userData) => {
      console.log(userData)
      socket.emit("setup",userData);  
      socket.join(userData._id);
      socket.emit("connected");

    });

    socket.on('user_connected', function (userId) {
    var singleUser = {};
    singleUser["socketId"] = socket.id;
    singleUser["userId"] = userId;
 
    var index = usersOnline.findIndex(obj =>obj.socketId == socket.id);
    if(index === -1){
    usersOnline.push(singleUser);
  
    io.emit("updateUserStatus", usersOnline);}
    console.log(usersOnline)
  });

    

    socket.on('disconnect', function () {
      
      var index = usersOnline.findIndex(obj =>obj.socketId == socket.id);
    if(index > -1){
    usersOnline.splice(index,1);
  
    io.emit("updateUserStatus", usersOnline);

     console.log("user Disconnected")
     console.log(usersOnline)
  }});
  
    socket.on("join chat", (room) => {
      socket.join(room);
      console.log("User Joined Room: " + room);
    });
    socket.on("typing", (room) => { socket.to(room).emit("typing", room); });
    socket.on("stop typing", (room) => socket.to(room).emit("stop typing"));
  
    socket.on("new message", (newMessageRecieved) => {
      var chat = newMessageRecieved.chat;
  
      if (!chat.users) return console.log("chat.users not defined");
  
      chat.users.forEach((user) => {
        if (user._id == newMessageRecieved.sender._id) return;
  
        socket.in(user._id).emit("message recieved", newMessageRecieved);
      });
    });
  
    socket.off("setup", () => {
      console.log("USER DISCONNECTED");
      socket.leave(userData._id);
    });
  });


